import slack
from os import environ as env
from dotenv import load_dotenv, find_dotenv
from flask import Flask, request, Response

from string import ascii_uppercase
import pandas as pd
import plotly.graph_objects as go

from waitress import serve

load_dotenv(dotenv_path=find_dotenv())

app = Flask(__name__)

client = slack.WebClient(token=env.get('SLACK_TOKEN'))
BOT_ID = client.api_call("auth.test").get("bot_id")


@app.route('/calculate-budget', methods=['POST'])
def calculate_budget():
    data = request.form
    channel_id = data.get('channel_id')
    campaign_name = data.get('text').strip().upper()

    # For security Reasons
    if campaign_name not in ascii_uppercase:
        pass
    else:
        try:
            df = pd.read_excel(f"data/{campaign_name}.xlsx")
        except FileNotFoundError:
            client.chat_postMessage(
                channel=channel_id, text=f'Requested campaign name "{campaign_name}" does not exist.')
            return Response(), 200
        total_budget = df['Total Budget'].values[0]
        client.chat_postMessage(
            channel=channel_id, text=f"Campaign {campaign_name}'s Total Budget is: {total_budget}")

    return Response(), 200


@app.route('/Bot-CompareCampaigns', methods=['POST'])
def Bot_CompareCampaigns():
    data = request.form
    channel_id = data.get('channel_id')

    campaign_names = data.get('text').strip().upper().split()

    impressions = []
    clicks = []
    installs = []
    for campaign_name in campaign_names:
        try:
            df = pd.read_excel(f'data/{campaign_name}.xlsx')
        except FileNotFoundError:
            client.chat_postMessage(
                channel=channel_id, text=f'Requested campaign name "{campaign_name}" does not exist.')
            return Response(), 200

        impression = df["Total Impression"].values[0]
        click = df["Total Clicks"].values[0]
        install = df["Total App Install"].values[0]

        impressions.append(impression)
        clicks.append(click)
        installs.append(install)

    fig = go.Figure()
    fig.add_trace(go.Bar(
        x=campaign_names,
        y=impressions,
        name='Impression',
        marker_color='indianred'
    ))
    fig.add_trace(go.Bar(
        x=campaign_names,
        y=clicks,
        name='Click',
        marker_color='lightsalmon'
    ))
    fig.add_trace(go.Bar(
        x=campaign_names,
        y=installs,
        name='Install',
        marker_color='crimson'
    ))

    fig.update_layout(barmode='group')
    fig.show()
    fig.write_image(file="data/Bot_CompareCampaigns.png")

    client.files_upload(file="data/Bot_CompareCampaigns.png", channels=channel_id,
                        filename="_".join(campaign_names)+"-Campaigns_Comparison.png")

    return Response(), 200


if __name__ == "__main__":
    # app.run(debug=True, host='0.0.0.0', port=80)
    serve(app, host='0.0.0.0', port=80)
