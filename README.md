# Case Study

This project was created for **Alictus Software Developer (Tools and Solutions) Case Study** by Ömer Furkan İşleyen.

### Main.py does the followings:

- Creates a folder that contains the data in Google Drive via API.
- Uploads the dataset to Google Drive via Google Drive API in spreadsheet format.
- Creates separate spreadsheet files for each campaign row in the dataset via Google Sheets API. For each campaign, creates a new column called **Total Budget** (total budget = total clicks \* cpc). Calculates the total budget and updates the cell accordingly.

### How to run:

Firstly, you should install the requirements.

```python
pip install -r requirements.txt
```

Then, follow along [this](https://developers.google.com/drive/api/v3/quickstart/python#prerequisites). You should create a project, enable the **Drive API** and **Google Sheets API**, create your credentials and save it as _credentials.json_.

After you obtain _credentials.json_ in the root directory, simply run the **Main.py** file, and it will create tokens.json and do above.

```python
python Main.py
```

On Mac and Linux:

```python
python3 Main.py
```

---

### What is Slack_Bot.py?

This file is just a Slack Bot application and has these **Slash Commands**:

- /calculate-budget: Takes a parameter _campaign name_ and returns the total budget based on the chosen campaign name as a reply.
- /bot_comparecampaigns: Takes two or more parameters split by whitespace as _campaign name_ and converts campaign results into charts that have different colors. Each campaign result chart includes _impressions_, _click_, and _install data_. Returns the png version of charts as a channel post.

### How to run:

Firstly, you should create a [slack bot application](https://api.slack.com/), then create your credentials and save them inside the .env file.

Then run the Slack_Bot.py file.

```python
python Slack_Bot.py
```

In Mac and Linux:

```python
python3 Slack_Bot.py
```
