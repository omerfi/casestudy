import pandas as pd

import os.path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build, Resource
from googleapiclient.http import MediaFileUpload

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/drive']


class Main:
    def __init__(self) -> None:
        self.data_file_location = "data/Data.xlsx"
        self.main()

    def main(self):
        self.core()

        # Call the Drive v3 API

        # Part 1
        self.create_service_instance('drive')
        # Create Folder
        folder_id = self.create_folder(
            directory_name='Datas', delete_old_ones=True)

        # Upload File
        data_file_id = self.upload_file(
            file_location=self.data_file_location,
            uploaded_file_name="Data.xlsx"
        )

        # Upload File As Spreadsheet
        spreadsheet_file_id = self.upload_file_as_spreadsheet(
            file_location=self.data_file_location,
            uploaded_file_name="Data_Spreadsheet"
        )

        # Part 2
        self.create_service_instance('sheets')

        # Get Values In Spreadsheet
        rows = self.get_values_in_spreadsheet(
            spreadsheet_file_id, _range="A1:Z1000")

        # Update Rows
        rows[0].append('Total Budget')
        for row in rows[1:]:
            row.append(float(row[2]) * float(row[4]))

        _columns = rows[0]
        _rows = rows[1:]

        # Create Seperate Excel Files
        for row in _rows:
            df = pd.DataFrame([row], columns=_columns)
            df.to_excel(f"data/{row[0]}.xlsx", index=False)

        # Upload Each One To Drive As Spreadsheet
        self.create_service_instance(service_name='drive')
        _spreadsheet_ids = []
        for row in _rows:
            _spreadsheet_id = self.upload_file_as_spreadsheet(
                f"data/{row[0]}.xlsx", row[0])
            _spreadsheet_ids.append(_spreadsheet_id)

        # Format Each Spreadsheet
        self.create_service_instance(service_name='sheets')
        for _spreadsheet_id in _spreadsheet_ids:
            _sheet_id = self.get_sheet_id(_spreadsheet_id)
            self.format_header_cells(_spreadsheet_id, _sheet_id)
            self.format_value_cells(_spreadsheet_id, _sheet_id)
            self.format_cells_directions(_spreadsheet_id, _sheet_id)

        2

    def core(self):
        self.creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.json'):
            self.creds = Credentials.from_authorized_user_file(
                'token.json', SCOPES)
        # If there are no (valid) credentials available, let the user log in.
        if not self.creds or not self.creds.valid:
            if self.creds and self.creds.expired and self.creds.refresh_token:
                self.creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                self.creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.json', 'w') as token:
                token.write(self.creds.to_json())

        # self.service = self.create_service_instance('drive')
        # self.service: Resource = build('drive', 'v3', credentials=self.creds)

    def create_service_instance(self, service_name):
        version = None
        if service_name == 'drive':
            version = 'v3'
        elif service_name == 'sheets':
            version = 'v4'
        self.service: Resource = build(
            service_name, version, credentials=self.creds)
        return self.service

    def format_header_cells(self, spreadsheet_id, sheet_id):
        request_body = {
            "requests": [
                {
                    "repeatCell": {
                        "range": {
                            "sheetId": sheet_id,
                            "startRowIndex": 0,
                            "endRowIndex": 1
                        },
                        "cell": {
                            "userEnteredFormat": {
                                "horizontalAlignment": "CENTER",
                                "verticalAlignment": "MIDDLE",
                                "textFormat": {
                                    "fontSize": 12,
                                    "bold": True
                                }
                            }
                        },
                        "fields": "userEnteredFormat(horizontalAlignment,verticalAlignment,textFormat)"
                    },
                }
            ]
        }
        response = self.service.spreadsheets().batchUpdate(
            spreadsheetId=spreadsheet_id,
            body=request_body
        ).execute()
        return response

    def format_value_cells(self, spreadsheet_id, sheet_id):
        request_body = {
            "requests": [
                {
                    "repeatCell": {
                        "range": {
                            "sheetId": sheet_id,
                            "startRowIndex": 1,
                            "endRowIndex": 1000
                        },
                        "cell": {
                            "userEnteredFormat": {
                                "horizontalAlignment": "CENTER",
                                "verticalAlignment": "MIDDLE"
                            }
                        },
                        "fields": "userEnteredFormat(horizontalAlignment,verticalAlignment)"
                    },
                }
            ]
        }
        response = self.service.spreadsheets().batchUpdate(
            spreadsheetId=spreadsheet_id,
            body=request_body
        ).execute()
        return response

    def format_cells_directions(self, spreadsheet_id, sheet_id):
        request_body = {
            "requests": [
                {
                    "autoResizeDimensions": {
                        "dimensions": {
                            "sheetId": sheet_id,
                            "dimension": "COLUMNS",
                            "startIndex": 0,
                            "endIndex": 1000
                        }
                    }
                }
            ]
        }
        response = self.service.spreadsheets().batchUpdate(
            spreadsheetId=spreadsheet_id,
            body=request_body
        ).execute()
        return response

    def get_spreadsheet_properties(self, spreadsheet_id):
        result = self.service.spreadsheets().get(
            spreadsheetId=spreadsheet_id,
        ).execute()
        return result

    def get_sheet_id(self, spreadsheet_id):
        result = self.get_spreadsheet_properties(spreadsheet_id)
        sheet_id = result.get('sheets')[0].get('properties').get('sheetId')
        return sheet_id

    def update_values_in_spreadsheet(self, spreadsheet_id, _range, rows):
        value_input_option = "RAW"
        body = {
            'values': rows
        }
        result = self.service.spreadsheets().values().update(
            spreadsheetId=spreadsheet_id, range=_range, valueInputOption=value_input_option, body=body).execute()
        return result

    def get_values_in_spreadsheet(self, spreadsheet_id, _range):
        result = self.service.spreadsheets().values().get(
            spreadsheetId=spreadsheet_id, range=_range).execute()
        rows = result.get('values', [])
        return rows

    def create_folder(self, directory_name, delete_old_ones=True):
        if delete_old_ones:
            # Search for directory
            results = self.service.files().list(
                q="mimeType='application/vnd.google-apps.folder'",
                fields="nextPageToken, files(id, name)"
            ).execute()
            items = results.get('files', [])

            # Get directory ids
            directory_name_ids = []
            for item in items:
                # print(u'{0} ({1})'.format(item['name'], item['id']))
                if item['name'] == directory_name:
                    directory_name_ids.append(item['id'])

            # Delete folders
            for dir_id in directory_name_ids:
                results = self.service.files().delete(
                    fileId=dir_id
                ).execute()

        # Create folder
        file_metadata = {
            'name': directory_name,
            'mimeType': 'application/vnd.google-apps.folder'
        }
        file = self.service.files().create(
            body=file_metadata,
            fields="id"
        ).execute()
        self.folder_id = file.get('id')
        return self.folder_id

    def get_mimetype(self, file: str):
        if file.endswith('.xlsx'):
            mimetype = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        elif file.endswith('.xls'):
            mimetype = "application/vnd.ms-excel"
        elif file.endswith('.csv'):
            mimetype = "text/csv"
        else:
            raise Exception(
                "Unsupported file extension, supported extensions are: .xlsx, .xls, .csv")
        return mimetype

    def upload_file(self, file_location, uploaded_file_name):
        file_metadata = {
            'name': uploaded_file_name,
            'parents': [self.folder_id]
        }
        mimetype = self.get_mimetype(file_location)
        media = MediaFileUpload(file_location, mimetype=mimetype)
        file = self.service.files().create(
            body=file_metadata,
            media_body=media,
            fields='id'
        ).execute()
        file_id = file.get('id')
        return file_id

    def upload_file_as_spreadsheet(self, file_location, uploaded_file_name):
        file_metadata = {
            'name': uploaded_file_name,
            'parents': [self.folder_id],
            'mimeType': 'application/vnd.google-apps.spreadsheet'
        }
        media = MediaFileUpload(
            file_location,
            mimetype=self.get_mimetype(file_location),
            resumable=True
        )
        file = self.service.files().create(
            body=file_metadata,
            media_body=media,
            fields='id'
        ).execute()
        spreadsheet_file_id = file.get('id')
        return spreadsheet_file_id


if __name__ == '__main__':
    Main()
